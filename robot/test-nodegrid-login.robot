*** Settings ***
Library    Selenium2Library
Library    String

Suite Setup			SUITE:Setup
Suite Teardown		SUITE:Teardown

*** Variables ***
${BROWSER}				headlessfirefox
# ${BROWSER}				firefox
${HOME_URL}				https://192.168.2.90

${USERNAME}            admin
${PASSWORD}            adminzpe2022

${TITLE_PAGE_LOGIN}			Nodegrid
${FIELD_USERNAME}        //input[@id='username']
${FIELD_PASSWORD}        //input[@id='password']
${BUTTON_LOGIN}            //input[@id='login-btn']
${BUTTON_CONSOLE}            //a[contains(text(),'Console')]
${TITLE_PAGE_ACCESS}    VM-2-90 - Nodegrid

*** Test Case ***
Test Access Page
	Go To								${HOME_URL}
	Wait Until Element Is Visible		${FIELD_USERNAME}    timeout=10
	Title Should Be 					${TITLE_PAGE_LOGIN}
	Capture Page Screenshot

Test Login
	Input Text		             ${FIELD_USERNAME}	${USERNAME}
	Input Text		             ${FIELD_PASSWORD}	${PASSWORD}
	Click Element                ${BUTTON_LOGIN}
	Wait Until Page Contains Element        ${BUTTON_CONSOLE}    timeout=10
	Page Should Contain Element			    ${BUTTON_CONSOLE}
	Title Should Be 			    ${TITLE_PAGE_ACCESS}
	Capture Page Screenshot

*** Keywords ***
SUITE:Setup
	Open Browser	about:blank		${BROWSER}

SUITE:Teardown
	close all browsers
